# About

## Usage for Development with live reloading

-   From the project directory enter the following commands:

```cli
$ npm install
$ npm start
```

-   Navigate to `http://localhost:8080/`.
-   Open your browser's development tools (usually F12).
-   Click the header text and at the same time check the console.

## Usage for building your project 

```cli
$ npm run build
```
Build (with sourcemaps) will be available in the `/dist` folder  

## When ready to 'deploy'

```cli
$ npm run prod
```
Production ready build (with sourcemaps & minified code) will be available in the `/dist` folder

## Technologies used
-   npm
-   webpack
    - typescript
    - loaders for css, fonts, etc.
    - plugin for HTML template
    - webpack-dev-server
